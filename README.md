# myTunes
myTunes is a Thymeleaf webpage and Spring API written in Java that can retrieve random music and genres from a database as well as allow the user to search for tracks using a search bar. On top of this, there is also a public API available for clients to use. This API can get all customers from the database, get one specific customer by their ID, get their most popular songs etc. 
These endpoints can be accessed by using `website/api/`

For a full list of all available endpoints, a JSON file was generated that can be imported inside Postman, which will display all the requests that can be performed. It is located in `/src/main/resources/postman`
Find the file and import it inside Postman and all requests should show up in a collection for easy testing.

Made by Sultan Iljasov and Edris Afazli

## How to use

The website and the API can be accessed in two ways. Either by using [this](https://edris-sultan-mytunes.herokuapp.com) public domain or by compiling the source code locally on your machine and accessing it using `localhost:8080` 

# Database
The database used is the Chinook database that provides some generic tables for learning and testing purposes. It is a good alternative to the Northwind database. They also both share similar tables which makes it easy to swap or add extra databases for further expansion.

