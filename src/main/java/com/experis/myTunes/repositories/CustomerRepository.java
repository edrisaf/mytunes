package com.experis.myTunes.repositories;

import com.experis.myTunes.models.CustomerCountry;
import com.experis.myTunes.models.Customer;
import com.experis.myTunes.models.CustomerGenre;
import com.experis.myTunes.models.CustomerInvoice;

import java.sql.*;
import java.util.ArrayList;

//This class is responsible for performing all API endpoint related tasks
//A lot of the functions here will look very similar, because they have to retrieve a lot of the same information
//Most of the time, the SQL query will be the main difference.
public class CustomerRepository
{
    String URL = ConnectionHelper.URL;
    private Connection connection = null;

    //Retrieves all customers from the Customer table
    public ArrayList<Customer> getCustomers()
    {
        ArrayList<Customer> customers = new ArrayList<>();
        try
        {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                ));
            }
            System.out.println("CustomerRepository: Successfully retrieved all customers.");
        }
        catch (Exception ex)
        {
            System.out.println("CustomerRepository: Something went wrong when getting all customers.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception ex){
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    //Retrieves a specific customer from the Customer table
    public Customer getCustomerById(int id)
    {
        Customer result = null;
        try
        {
            // Connect to DB
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");
            // Make SQL query
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email FROM Customer WHERE CustomerId = ?");
            preparedStatement.setInt(1,id);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                result = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                        );
            }
            System.out.println("CustomerRepository: Successfully selected a specific customer.");
        }
        catch (Exception exception)
        {
            System.out.println(exception.toString());
            System.out.println("CustomerRepository: Something went wrong when getting one customer.");
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception exception)
            {
                System.out.println(exception.toString());
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
            }
        }
        return result;
    }

    //Places a new customer into the Customer table
    public Boolean addCustomer(Customer customer)
    {
        if (customer == null) return false;
        boolean success = false;
        try
        {
            // Connect to DB
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            // Make SQL query
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO Customer(CustomerId,FirstName,LastName,Country,PostalCode,Phone,Email) VALUES(?,?,?,?,?,?,?)");
            preparedStatement.setInt(1,customer.getCustomerId());
            preparedStatement.setString(2,customer.getFirstName());
            preparedStatement.setString(3,customer.getLastName());
            preparedStatement.setString(4,customer.getCountry());
            preparedStatement.setString(5,customer.getPostalCode());
            preparedStatement.setString(6,customer.getPhone());
            preparedStatement.setString(7,customer.getEmail());
            // Execute Query
            int result = preparedStatement.executeUpdate();
            success = (result != 0);
            System.out.println("CustomerRepository: Successfully added a customer.");
        }
        catch (Exception exception)
        {
            System.out.println(exception.toString());
            System.out.println("CustomerRepository: Something went wrong when adding a customer.");
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception exception)
            {
                System.out.println(exception.toString());
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
            }
        }
        return success;
    }

    //Updates an existing customer inside the Customer table
    public Boolean updateCustomer(int id, Customer customer)
    {
        if (customer == null) return false;
        boolean success = false;
        try
        {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("UPDATE customer SET CustomerId = ?, FirstName = ?, LastName = ?, Country = ?, PostalCode = ?, Phone = ?, Email = ? WHERE CustomerId = ?");
            preparedStatement.setInt(1, customer.getCustomerId());
            preparedStatement.setString(2, customer.getFirstName());
            preparedStatement.setString(3, customer.getLastName());
            preparedStatement.setString(4, customer.getCountry());
            preparedStatement.setString(5, customer.getPostalCode());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getEmail());
            preparedStatement.setInt(8, id);

            // Execute Statement
            int result = preparedStatement.executeUpdate();

            success = (result != 0);

            System.out.println("CustomerRepository: Successfully updated a customer.");
        }
        catch (Exception ex)
        {
            System.out.println("CustomerRepository: Something went wrong when updating a customer.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                // Close Connection
                connection.close();
            }
            catch (Exception ex)
            {
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    //Groups all customers by their country and orders them by descending order
    public ArrayList<CustomerCountry> orderCustomersByCountry()
    {
        ArrayList<CustomerCountry> countries = new ArrayList<>();
        try
        {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(CustomerId), Country FROM Customer GROUP BY Country ORDER BY COUNT(CustomerId) DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countries.add(new CustomerCountry(
                        resultSet.getString("Country"),
                        resultSet.getInt("COUNT(CustomerId)")
                ));
            }
            System.out.println("CustomerRepository: Successfully counted customers by country and ordered by descending.");
        }
        catch (Exception ex)
        {
            System.out.println("CustomerRepository: Something went wrong when counting customers by country.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception ex){
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return countries;
    }

    //Deletes a specific customer from the Customer table
    public Boolean deleteCustomer(int Id)
    {
        boolean success = false;
        try {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");


            // Prepare Statement
            PreparedStatement preparedStatement =
                    connection.prepareStatement("DELETE FROM Customer WHERE CustomerId = ?");
            preparedStatement.setInt(1, Id);
            // Execute Statement
            int result = preparedStatement.executeUpdate();

            success = (result != 0);

            System.out.println("CustomerRepository: Successfully deleted a customer.");
        }
        catch (Exception ex){
            System.out.println("CustomerRepository: Something went wrong when deleting a customer.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception ex)
            {
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return success;
    }

    //Orders the Invoice table by the highest spenders
    public ArrayList<CustomerInvoice> getHighestSpenders()
    {

        ArrayList<CustomerInvoice> spenders = new ArrayList<>();
        try
        {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT SUM(I.Total) AS SUM, C.FirstName, C.LastName FROM [Invoice] I JOIN Customer C ON I.CustomerId = C.CustomerId GROUP BY C.FirstName, C.LastName ORDER BY SUM(I.Total) DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spenders.add(new CustomerInvoice(
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getDouble("SUM")
                ));
            }
            System.out.println("CustomerRepository: Successfully collected all the highest spenders and ordered by descending.");
        }
        catch (Exception ex)
        {
            System.out.println("CustomerRepository: Something went wrong when getting highest spenders.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception ex){
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return spenders;
    }

    //Performs multiple table joins to find what genre a specific user has purchased the most
    public ArrayList<CustomerGenre> getCustomerPopularGenre(int id)
    {
        ArrayList<CustomerGenre> popular = new ArrayList<CustomerGenre>();
        try
        {
            // Open Connection
            connection = DriverManager.getConnection(URL);
            System.out.println("Connected to the database.");

            PreparedStatement preparedStatement =
                    connection.prepareStatement("SELECT MAX(GenreTotalTracks) AS GenreTracks, GenreName, First, Last FROM (SELECT C.FirstName AS First, C.LastName AS Last, G.Name AS GenreName, COUNT(G.GenreId) AS GenreTotalTracks FROM Invoice I JOIN InvoiceLine L ON L.InvoiceId = I.InvoiceId JOIN Track T ON T.TrackId = L.TrackId JOIN Genre G ON G.GenreId = T.GenreId JOIN Customer C ON I.CustomerId = C.CustomerId WHERE I.CustomerId = ? GROUP BY G.GenreId ORDER BY GenreTotalTracks DESC)");
            preparedStatement.setInt(1,id);
            // Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                popular.add(new CustomerGenre(
                        resultSet.getString("First"),
                        resultSet.getString("Last"),
                        resultSet.getString("GenreName")
                ));
            }
            System.out.println("CustomerRepository: Successfully collected the most popular genres for a customer.");
        }
        catch (Exception ex)
        {
            System.out.println("CustomerRepository: Something went wrong when getting most popular for customer.");
            System.out.println(ex.toString());
        }
        finally
        {
            try
            {
                connection.close();
            }
            catch (Exception ex){
                System.out.println("CustomerRepository: Something went wrong while closing the connection.");
                System.out.println(ex.toString());
            }
        }
        return popular;
    }
}
