package com.experis.myTunes.repositories;

import com.experis.myTunes.models.Genre;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class GenreRepository {
    // Setting up the connection object
    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    // Method to fetch five random genres from database
    public ArrayList<Genre> getRandomGenres(){
        ArrayList<Genre> genres = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            // prepare statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT GenreId, Name FROM genre ORDER BY RANDOM() LIMIT 5");

            // execute
            ResultSet set = preparedStatement.executeQuery();

            // populate genres list
            while(set.next()){
                genres.add(
                        new Genre(set.getString("GenreId"), set.getString("Name")));
            }
            System.out.println("Selecting random genres successful!");

        } catch (Exception ex) {
            System.out.println("Something went wrong while selecting random genres!");
            ex.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch(Exception ex) {
                System.out.println("Something went wrong while closing the connection.");
                ex.printStackTrace();
            }
        }
        return genres;
    }
}
