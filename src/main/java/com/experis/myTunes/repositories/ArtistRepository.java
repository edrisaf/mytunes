package com.experis.myTunes.repositories;

import com.experis.myTunes.models.Artist;

import java.sql.*;
import java.util.ArrayList;

public class ArtistRepository {
    // Setting up the connection object
    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    // Method to fetch five random artists from database and store in "artists" list
    public ArrayList<Artist> getRandomArtists(){
        ArrayList<Artist> artists = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            // prepare statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT ArtistId, Name FROM artist ORDER BY RANDOM() LIMIT 5");

            // execute
            ResultSet set = preparedStatement.executeQuery();

            // populate artists list
            while(set.next()){
                artists.add(
                        new Artist(set.getString("ArtistId"), set.getString("Name")));
            }
            System.out.println("Selecting random artists successful!");

        } catch (Exception ex) {
            System.out.println("Something went wrong while selecting random artists!");
            ex.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch(Exception ex) {
                System.out.println("Something went wrong while closing the connection.");
                ex.printStackTrace();
            }
        }
        return artists;
    }
}
