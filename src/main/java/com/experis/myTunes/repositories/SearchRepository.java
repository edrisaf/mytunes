package com.experis.myTunes.repositories;

import com.experis.myTunes.models.Artist;
import com.experis.myTunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SearchRepository {
    // Setting up the connection object
    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    // Method to
    public ArrayList<Track> getSearchResults(String search) {
        ArrayList<Track> searchResults = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            // prepare statement: select names of track, artist, album and genre through "inner join" based on input
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Track.name as trackName, Artist.Name as artistName, Album.Title as albumName, Genre.Name as genreName FROM (((Track "+
                            "INNER JOIN Artist ON Album.ArtistId = Artist.artistid)"+
                            "INNER JOIN Album ON Track.albumid = Album.albumid)"+
                            "INNER JOIN Genre ON Track.genreid = Genre.genreid) WHERE Track.Name LIKE ?");

            // set the search word(s) parameter: track name contains the "search" parameter
            preparedStatement.setString(1,"%" + search + "%");

            // execute
            ResultSet set = preparedStatement.executeQuery();

            // populate searchResults list
            while (set.next()) {
                searchResults.add(
                        new Track(
                                set.getString("trackName"),
                                set.getString("artistName"),
                                set.getString("albumName"),
                                set.getString("genreName")));
            }
            System.out.println("Search successful!");

        } catch (Exception ex) {
            System.out.println("Something went wrong while searching for track!");
            ex.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing the connection.");
                ex.printStackTrace();
            }
        }
        return searchResults;
    }
}
