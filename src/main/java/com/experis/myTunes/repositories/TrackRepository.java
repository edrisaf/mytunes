package com.experis.myTunes.repositories;

import com.experis.myTunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TrackRepository {

    // Setting up the connection object
    private String URL = ConnectionHelper.URL;
    private Connection conn = null;

    // Method to fetch five random tracks from database
    public ArrayList<Track> getRandomTracks(){
        ArrayList<Track> tracks = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(URL);

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT TrackId, Name FROM Track ORDER BY RANDOM() LIMIT 5");

            ResultSet set = preparedStatement.executeQuery();

            // populate tracks list
            while (set.next()){
                tracks.add(
                        new Track(
                                set.getString("TrackId"),
                                set.getString("Name"))
                );
            }
            System.out.println("Selecting random tracks successful!");
        }catch (Exception ex){
            System.out.println("Something went wrong while selecting random tracks!");
            ex.printStackTrace();
        }
        finally {
            try {
                conn.close();
            } catch (Exception ex){
                System.out.println("Something went wrong while closing the connection.");
                ex.printStackTrace();
            }
        }
        return tracks;
    }

}
