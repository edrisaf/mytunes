package com.experis.myTunes.models;

//POJO for CustomerCountry. Stores all data we need to display customer counts for each country
public class CustomerCountry
{
    public String Country;
    public int CustomerCount;
    public CustomerCountry(String Name, int CustomerCount)
    {
        this.Country = Name;
        this.CustomerCount = CustomerCount;
    }
}