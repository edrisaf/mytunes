package com.experis.myTunes.models;

//POJO for CustomerInvoice. Stores all data we need to represent a customer and their spendings
public class CustomerInvoice
{
    public String FirstName;
    public String LastName;
    public double TotalSpent;

    public CustomerInvoice(String firstName, String lastName, double totalSpent)
    {
        FirstName = firstName;
        LastName = lastName;
        TotalSpent = totalSpent;
    }
}
