package com.experis.myTunes.models;

public class Track
{
    private String TrackId;
    private String Name;
    private String artist;
    private String album;
    private String genre;

    // Created two constructors for this class, allowing us to use it for both TrackRepository and SearchRepository
    public Track(String trackId, String name) {
        TrackId = trackId;
        Name = name;
    }

    public Track(String name, String artist, String album, String genre){
        this.Name = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getTrackId() {
        return TrackId;
    }

    public void setTrackId(String trackId) {
        TrackId = trackId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

}
