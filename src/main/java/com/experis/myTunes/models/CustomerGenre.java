package com.experis.myTunes.models;

//POJO for CustomerGenre. Stores all data we need to display a customer's most popular genre
public class CustomerGenre
{
    public String FirstName;
    public String LastName;
    public String Genre;
    public CustomerGenre(String firstName, String lastName, String genre)
    {
        FirstName = firstName;
        LastName = lastName;
        Genre = genre;
    }
}
