package com.experis.myTunes.controllers;

import com.experis.myTunes.models.CustomerCountry;
import com.experis.myTunes.models.Customer;
import com.experis.myTunes.models.CustomerGenre;
import com.experis.myTunes.models.CustomerInvoice;
import com.experis.myTunes.repositories.CustomerRepository;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

//Will expose our customer stuff to various endpoints
@CrossOrigin
@RestController
public class CustomerController
{
    //Initializing the repository we're going to use
    private CustomerRepository customerRep = new CustomerRepository();

    // Mapping web request "api/customer/id" to the repository. When called, returns a customer with the specified ID
    @RequestMapping(value = "api/customers/{id}", method = RequestMethod.GET)
    public Customer getCustomerById(@PathVariable int id)
    {
        return customerRep.getCustomerById(id);
    }

    // Mapping web request "api/customers" to the repository. When called, returns all customers
    @RequestMapping(value = "api/customers", method = RequestMethod.GET)
    public ArrayList<Customer> getCustomers()
    {
        return customerRep.getCustomers();
    }

    // Mapping web request "api/customer/add" to the repository. When called, adds a new customer to the Customer table
    @RequestMapping(value = "api/customers/add", method = RequestMethod.POST)
    public String addNewCustomer(@RequestBody Customer customer)
    {
        boolean success = customerRep.addCustomer(customer);
        if (success) return "Customer was added successfully";
        else return "Error, could not add new customer.";
    }

    // Mapping web request "api/customer/id/update" to the repository. When called, updates an existing customer inside the Customer table
    @RequestMapping(value = "api/customers/{id}/update", method = RequestMethod.PUT)
    public String updateExistingCustomer(@PathVariable int id, @RequestBody Customer customer)
    {
        boolean success = customerRep.updateCustomer(id, customer);
        if (success) return "Customer with ID " + id +  " was updated.";
        else return "Error, could not update customer.";
    }

    // Mapping web request "api/customer/country" to the repository. When called, groups all countries and returns how many customers there are in every country
    @RequestMapping(value = "api/customers/country", method = RequestMethod.GET)
    public ArrayList<CustomerCountry> getCountryCustomers()
    {
        return customerRep.orderCustomersByCountry();
    }

    // Mapping web request "api/customer/id/delete" to the repository. When called, deletes a customer with the specified ID inside the Customer table
    @RequestMapping(value = "api/customers/{id}/delete", method = RequestMethod.DELETE)
    public String deleteCustomerById(@PathVariable int id)
    {
        boolean success = customerRep.deleteCustomer(id);
        if (success) return "Customer with ID " + id +  " was deleted.";
        else return "Error, could not delete customer.";
    }

    // Mapping web request "api/customer/spenders" to the repository. When called, returns all the customers who are the highest spenders inside the Invoice table
    @RequestMapping(value = "api/customers/spenders", method = RequestMethod.GET)
    public ArrayList<CustomerInvoice> getHighestSpenders()
    {
        return customerRep.getHighestSpenders();
    }

    // Mapping web request "api/customer/id/popular/genre" to the repository. When called, returns the specified customer ID's most popular genre or genres
    @RequestMapping(value = "api/customers/{id}/popular/genre", method = RequestMethod.GET)
    public ArrayList<CustomerGenre> getCustomerPopularGenre(@PathVariable int id)
    {
        return customerRep.getCustomerPopularGenre(id);
    }
}
