package com.experis.myTunes.controllers;

import com.experis.myTunes.repositories.ArtistRepository;
import com.experis.myTunes.repositories.GenreRepository;
import com.experis.myTunes.repositories.SearchRepository;
import com.experis.myTunes.repositories.TrackRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MusicController {
    // Initialize repositories
    private final TrackRepository trackRepository = new TrackRepository();
    private ArtistRepository artistRepository = new ArtistRepository();
    private GenreRepository genreRepository = new GenreRepository();
    private SearchRepository searchRepository = new SearchRepository();

    // Mapping web request "/" to the home controller. When called, generates five random tracks, artists, genres and
    // add them to the model. Finally, return index to view index.html page
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(Model model){
        model.addAttribute("tracks", trackRepository.getRandomTracks());
        model.addAttribute("artists", artistRepository.getRandomArtists());
        model.addAttribute("genres", genreRepository.getRandomGenres());
        return "index";
    }

    // Mapping web request "/search" to the search controller. When called, uses the requestparam to search for tracks
    // in the database, and adds the search results and term as model attributes
    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam("term") String term, Model model){
        model.addAttribute("term", term);
        model.addAttribute("results", searchRepository.getSearchResults(term));
        return "search";
    }
}
