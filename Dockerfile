FROM openjdk:15
ADD target/myTunes-0.0.1-SNAPSHOT.jar mytunes-assignment.jar
ENTRYPOINT ["java", "-jar", "/mytunes-assignment.jar"]